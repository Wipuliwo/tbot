package com.tibiabot.core.presentation.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.tibiabot.core.service.EventListenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MenuScreen extends BasicScreen {

    private static final int btnSize = 100;
    private static final int LOGOWIDTH = 200;
    private static final int LOGOHEIGHT = 100;

    @Autowired
    private
    FeaturesScreen featuresScreen;

    @Override
    public void resize(final int i, final int i1) {

    }

    @Override
    public void show() {
        super.show();

        Texture botLogo = new Texture("tibia.png");
        Image image = new Image(botLogo);


        TextButton login = new TextButton("Login", this.skin);
        TextButton register = new TextButton("Register", this.skin);

        TextField username = new TextField("username", this.skin);
        TextField password = new TextField("password", this.skin);

        password.setPosition(passwordX,passwordY);
        username.setPosition(usernameX, usernameY);

        image.setSize(200, 100);

        register.setSize(100, 40);
        login.setSize(100, 40);

        login.setPosition(loginX, loginY);
        register.setPosition(registerX, registerY);

        image.setPosition(botLogoX, botLogoY);

        login.addListener(this.eventListenerService.getLoginClickEvent(this.featuresScreen, this, username, password));

        this.stage.addActor(image);
        this.stage.addActor(login);
        this.stage.addActor(register);
        this.stage.addActor(username);
        this.stage.addActor(password);
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
