package com.tibiabot.core.presentation.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.tibiabot.core.presentation.TibiaBot;
import com.tibiabot.core.service.EventListenerService;
import com.tibiabot.core.service.ScreenChangerService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BasicScreen implements BasicScreenGraphics, Screen {

    @Autowired
    EventListenerService eventListenerService;

    Skin skin;
    private Button closeBtn;
    Stage stage;
    private Texture background;
    Label title;
    private Dialog dialog;

    @Override
    public void render(final float v) {

        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        this.stage.getBatch().begin();
        this.stage.getBatch().draw(this.background,0,0, TibiaBot.size,TibiaBot.size);
        this.stage.getBatch().end();
        this.stage.act();
        this.stage.draw();
    }

    @Override
    public void show() {

        this.skin = new Skin(Gdx.files.internal("sgx/skin/sgx-ui.json"));
        this.background = new Texture("niceBackground.jpg");
        this.stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(this.stage);

        this.closeBtn = new Button(this.skin, "close");
        this.closeBtn.addListener(this.eventListenerService.getGobackClickEvent());

        this.closeBtn.setPosition(closeX, closeY);
        this.stage.addActor(this.closeBtn);
        this.dialog = new Dialog("Error", this.skin);
        this.dialog.button("Ok");

    }

    @Override
    public void resize(final int i, final int i1) {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    public void errorDialogGenerator(String message){
        Label label = new Label(message, this.skin, "small");
        this.dialog.getContentTable().clear();
        this.dialog.text(label);
        this.dialog.show(this.stage);
    }
}
