package com.tibiabot.core.presentation.screens;

public interface BasicScreenGraphics {

     static final int row = 400/8;
     static final int col = 400/8;

    static final int hpManaScreenTitleX = 90;
    static final int hpManaScreenTitleY = 350;
     static final int startHpY = 300;
     static final int startHpX = 200;
     static final int endHpY = 250;
     static final int endHpX = startHpX;
     static final int startManaY = 150;
     static final int startManaX = startHpX;
     static final int endManaY = 100;
     static final int endManaX = startHpX;
     static final int hpHotkeyX = 284;
    static final int hpHotkeyY = 273;
    static final int hpBehaveNameX = 100;
    static final int hpBehaveNameY = hpHotkeyY;
    static final int manaHotkeyX = 284;
    static final int manaHotkeyY = 124;
    static final int manaBehaveNameX = hpBehaveNameX;
    static final int getManaBehaveNameY = manaHotkeyY;
    static final int ammoHotkeyX = 284;
    static final int ammoHotkeyY = 124;
    static final int paralyzeHotkeyX= 284;
    static final int paralyzeHotkeyY = 199;
    static final int eaterHotkeyX = 284;
    static final int eaterHotkeyY = 273;
    static final int hpManalistX = 100;
    static final int hpManalistY = 200;
    static final int utilitiesListX = 150;
    static final int utilitiesListY = 80;
    static final int closeX = 25;
    static final int closeY = 375;
    static final int listBehavioursX = 95;
    static final int listBehavioursY = 250;
    static final int eaterNameX = eaterHotkeyX - 180;
    static final int eaterNameY = eaterHotkeyY;
    static final int ammoNameX = ammoHotkeyX - 180;
    static final int ammoNameY = ammoHotkeyY;
    static final int paralyzeNameX = paralyzeHotkeyX - 180;
    static final int paralyzeNameY = paralyzeHotkeyY;

    //menuscreen
    static final int registerX = 270;
    static final int registerY = 100 ;
    static final int loginX = registerX - 150;
    static final int loginY = 100;
    static final int botLogoX = 120 ;
    static final int botLogoY = 280;
    static final int usernameX = botLogoX ;
    static final int usernameY = botLogoY - 80 ;
    static final int passwordX = usernameX;
    static final int passwordY = usernameY - 50;


    static final int startFieldX = listBehavioursX;
    static final int startFieldY = listBehavioursY - 80;
    static final int endFieldX = listBehavioursX + 100;
    static final int endFieldY = startFieldY;
    static final int hotkeyListFieldX = endFieldX + 120;
    static final int hotkeyListFieldY = startFieldY;
    static final int saveX = listBehavioursX;
    static final int saveY = startFieldY - 80;
    static final int deleteX = saveX + 120 ;
    static final int deleteY = startFieldY - 80;



    static final int eaterLabelX = eaterHotkeyX + 10;
    static final int eaterLabelY = eaterHotkeyY + 30;
    static final int ammoLabelX = ammoHotkeyX + 10;
    static final int ammoLabelY = ammoHotkeyY + 30;
    static final int paralyzeLabelX = paralyzeHotkeyX + 3;
    static final int paralyzeLabelY = paralyzeHotkeyY + 30;

     static final int addHpX = hpHotkeyX + 50;
     static final int addHpY = hpHotkeyY;
     static final int addEaterX = eaterHotkeyX + 50;
     static final int addEaterY = eaterHotkeyY;
     static final int addAmmoX = ammoHotkeyX + 50;
     static final int addAmmoY = ammoHotkeyY;
     static final int addParalyzeX = paralyzeHotkeyX + 50;
     static final int addParalyzeY = paralyzeHotkeyY;
     static final int addManaX = manaHotkeyX + 50;
     static final int addManaY = manaHotkeyY;


}
