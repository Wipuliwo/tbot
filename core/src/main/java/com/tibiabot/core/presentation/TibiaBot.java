package com.tibiabot.core.presentation;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.tibiabot.core.engine.ammo.AmmoEngine;
import com.tibiabot.core.engine.eater.EaterEngine;
import com.tibiabot.core.engine.healer.workers.HealerEngine;
import com.tibiabot.core.engine.healer.workers.ManaEngine;
import com.tibiabot.core.engine.paralize.ParalyzeEngine;
import com.tibiabot.core.presentation.screens.MenuScreen;
import com.tibiabot.core.service.BehaviourService;
import com.tibiabot.core.state.BotState;
import com.tibiabot.core.threads.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class TibiaBot extends Game {

	public static final int size = 400;
	public static final int row = 400/8;
	public static final int col = 400/8;

	private Batch batch;

	@Autowired
	private
	BotState botState;

	@Autowired
	private
	BehaviourService behaviourService;

	@Autowired
	private MenuScreen menuScreen;

	@Autowired
	private HealerEngine healerEngine;
	@Autowired
	private AmmoEngine ammoEngine;
	@Autowired
	private EaterEngine eaterEngine;
	@Autowired
	private ParalyzeEngine paralyzeEngine;

	@Autowired
	private ManaEngine manaEngine;

	@Autowired
	private HealThread healThread;
	@Autowired
	private EatThread eaterThread;
	@Autowired
	private ManaThread manaThread;
	@Autowired
	private AmmoThread ammoThread;




	@Override
	public void create () {

		this.batch = new SpriteBatch();

		final Thread healThread = new Thread(this.healThread);
		healThread.setPriority(1);
		System.out.println("starting threads");
		healThread.start();

		final Thread manaThread = new Thread(this.manaThread);
		manaThread.setPriority(1);
		manaThread.start();

		final Thread eaterThread = new Thread(this.eaterThread);
		eaterThread.setPriority(1);
		eaterThread.start();

		final Thread ammoThread = new Thread(this.ammoThread);
		ammoThread.setPriority(2);
		ammoThread.start();

		this.batch = new SpriteBatch();
		this.setScreen(this.menuScreen);

		try {
			this.behaviourService.load();
		} catch (IOException | ClassNotFoundException e) {
			this.menuScreen.errorDialogGenerator("Could not load last state");
		}
	}

	@Override
	public void resize (final int width, final int height) {
	}

	@Override
	public void render () {

		super.render();

		 if (this.botState.healerIsOn()) {
			 this.healThread.setOn(true);
		 	synchronized (this.healThread) {
				this.healThread.notifyAll(); }
		 }else if (!this.botState.healerIsOn()) {
			 this.healThread.setOn(false);
		 }

		 if (this.botState.potterIsOn()){
			 this.manaThread.setOn(true);
		 	synchronized (this.manaThread) {
				this.manaThread.notifyAll(); }
		 }else if (!this.botState.potterIsOn()) {
			 this.manaThread.setOn(false);
		 }

		 if (this.botState.eaterIsOn()) {
			 this.eaterThread.setOn(true);
		 	synchronized (this.eaterThread) {
				this.eaterThread.notifyAll(); }
		 }else if (!this.botState.eaterIsOn()) {
			 this.eaterThread.setOn(false);
		 }

		 if (this.botState.ammoIsOn()) {
			 this.ammoThread.setOn(true);
		 	synchronized (this.ammoThread) {
				this.ammoThread.notifyAll(); }
		 } else if (!this.botState.ammoIsOn()) {
			 this.ammoThread.setOn(false);
		 }

	}

	@Override
	public void pause () {
	}

	@Override
	public void resume () {
	}

	@Override
	public void dispose () {
	}


	public Batch getBatch() {
		return this.batch;
	}


	public MenuScreen getMenu() {
		return this.menuScreen;
	}


}
