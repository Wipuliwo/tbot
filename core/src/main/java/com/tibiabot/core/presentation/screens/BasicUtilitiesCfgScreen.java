package com.tibiabot.core.presentation.screens;

import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.tibiabot.core.utils.Behaviours;
import org.springframework.beans.factory.annotation.Autowired;


public class BasicUtilitiesCfgScreen extends BasicScreen {

    @Autowired
    private
    ListBehavioursScreen listBehavioursScreen;

    @Override
    public void show() {

        super.show();

        this.title = new Label("Utilities Config", this.skin);
        final Label eaterLabel = new Label("Eater", this.skin, "small-white");
        final Label ammoLabel = new Label("Ammo", this.skin, "small-white");
        final Label paralyzeLabel = new Label("Paralyze", this.skin, "small-white");
        final TextButton utilitiesList = new TextButton("List", this.skin, "default");
        final TextField eaterHotkey = new TextField("HTK", this.skin, "spinner");
        final TextField ammoHotkey = new TextField("HTK", this.skin, "spinner");
        final TextField paralyzeHotkey = new TextField("HTK", this.skin, "spinner");
        final Button addEaterBehaviour = new Button(this.skin, "plus");
        final Button addAmmoBehaviour = new Button(this.skin, "plus");
        final Button addParalyzeBehaviour = new Button(this.skin, "plus");
        final TextField ammoName = new TextField("Name", this.skin, "spinner");
        final TextField eaterName = new TextField("Name", this.skin, "spinner");
        final TextField paralyzeName = new TextField("Name", this.skin, "spinner");

        //Listeners

        eaterHotkey.addListener(this.eventListenerService.getHtkInputListener(eaterHotkey));
        eaterHotkey.addListener(this.eventListenerService.getTextFieldClickListener(eaterHotkey));
        ammoHotkey.addListener(this.eventListenerService.getHtkInputListener(ammoHotkey));
        ammoHotkey.addListener(this.eventListenerService.getTextFieldClickListener(ammoHotkey));
        paralyzeHotkey.addListener(this.eventListenerService.getHtkInputListener(paralyzeHotkey));
        paralyzeHotkey.addListener(this.eventListenerService.getTextFieldClickListener(paralyzeHotkey));
        utilitiesList.addListener(this.eventListenerService.getScreenChangeClickEvent(this.listBehavioursScreen));
        eaterName.addListener(this.eventListenerService.getTextFieldClickListener(eaterName));
        ammoName.addListener(this.eventListenerService.getTextFieldClickListener(ammoName));
        paralyzeName.addListener(this.eventListenerService.getTextFieldClickListener(paralyzeName));
        addAmmoBehaviour.addListener(this.eventListenerService.getAddBehaveClickEvent(null, null, ammoHotkey, ammoName, Behaviours.AMMO, this));
        addEaterBehaviour.addListener(this.eventListenerService.getAddBehaveClickEvent(null, null, eaterHotkey, eaterName, Behaviours.EATER, this));
        addParalyzeBehaviour.addListener(this.eventListenerService.getAddBehaveClickEvent(null, null, paralyzeHotkey, paralyzeName, Behaviours.PARALYZE, this));

        //Size and position

        eaterHotkey.setWidth(50);
        ammoHotkey.setWidth(50);
        paralyzeHotkey.setWidth(50);
        utilitiesList.setWidth(utilitiesList.getWidth() + 50);

        this.title.setPosition(hpManaScreenTitleX, hpManaScreenTitleY);
        eaterLabel.setPosition(eaterLabelX, eaterLabelY);
        ammoLabel.setPosition(ammoLabelX, ammoLabelY);
        paralyzeLabel.setPosition(paralyzeLabelX, paralyzeLabelY);
        eaterHotkey.setPosition(eaterHotkeyX, eaterHotkeyY);
        ammoHotkey.setPosition(ammoHotkeyX, ammoHotkeyY);
        paralyzeHotkey.setPosition(paralyzeHotkeyX, paralyzeHotkeyY);
        utilitiesList.setPosition(utilitiesListX,utilitiesListY);
        addEaterBehaviour.setPosition(addEaterX, addEaterY);
        addAmmoBehaviour.setPosition(addAmmoX, addAmmoY);
        addParalyzeBehaviour.setPosition(addParalyzeX, addParalyzeY);
        paralyzeName.setPosition(paralyzeNameX, paralyzeNameY);
        ammoName.setPosition(ammoNameX, ammoNameY);
        eaterName.setPosition(eaterNameX,eaterNameY);

        this.stage.addActor(this.title);
        this.stage.addActor(eaterHotkey);
        this.stage.addActor(ammoHotkey);
        this.stage.addActor(paralyzeHotkey);
        this.stage.addActor(utilitiesList);
        this.stage.addActor(paralyzeLabel);
        this.stage.addActor(eaterLabel);
        this.stage.addActor(ammoLabel);
        this.stage.addActor(addEaterBehaviour);
        this.stage.addActor(addAmmoBehaviour);
        this.stage.addActor(addParalyzeBehaviour);
        this.stage.addActor(eaterLabel);
        this.stage.addActor(paralyzeName);
        this.stage.addActor(eaterName);
        this.stage.addActor(ammoName);

    }
}
