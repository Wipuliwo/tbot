package com.tibiabot.core.presentation.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.tibiabot.core.exception.BehaviourExists;
import com.tibiabot.core.engine.common.abstractions.Abehaviour;
import com.tibiabot.core.service.BehaviourService;
import com.tibiabot.core.service.ListScreenService;
import com.tibiabot.core.service.EventListenerService;
import com.tibiabot.core.utils.TbotUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.event.KeyEvent;
import java.io.IOException;

public class ListBehavioursScreen extends BasicScreen {

    @Autowired
    private
    FeaturesScreen featuresScreen;
    @Autowired
    private
    BehaviourService behaviourService;
    @Autowired
    private
    EventListenerService eventListenerService;
    @Autowired
    private ListScreenService listScreenService;

    private SelectBox<Object> listBehaviours;

    private TextField hotkeyField;
    private TextField startField;
    private TextField endField;

    private int selectedIndex;


    @Override
    public void render(float v) {

        if(this.listBehaviours.getSelectedIndex() != this.selectedIndex) {
            Abehaviour selectedBehaviour = this.listScreenService.getSelectedBehaviour(this.listBehaviours.getSelectedIndex());

            this.startField.setText(selectedBehaviour.getStartValue());
            this.endField.setText(selectedBehaviour.getEndValue());
            this.hotkeyField.setText(KeyEvent.getKeyText(selectedBehaviour.getHotkey()));
            this.selectedIndex = this.listBehaviours.getSelectedIndex();
        }
        super.render(v);
    }

    @Override
    public void show() {
        super.show();

        this.listBehaviours = new SelectBox(this.skin, "default");
        this.startField = new TextField("", this.skin);
        this.endField = new TextField("", this.skin);
        this.hotkeyField = new TextField("", this.skin);
        this.title = new Label("List of bot behaviours", this.skin, "default");
        final Label startLabel = new Label("start %", this.skin, "small");
        final Label endLabel = new Label("end %", this.skin, "small");
        final Label htkLabel = new Label("htk", this.skin, "small");
        final TextButton save = new TextButton("Save", this.skin, "default");
        final TextButton delete = new TextButton("Delete", this.skin, "default");

        this.listBehaviours.setItems(this.listScreenService.getAllBehaviourNames());

        //Listeners

        this.startField.addListener(this.eventListenerService.getTextFieldInputListener(this.startField));
        this.endField.addListener(this.eventListenerService.getTextFieldInputListener(this.endField));
        this.hotkeyField.addListener(this.eventListenerService.getHtkInputListener(this.hotkeyField));
        save.addListener(this.eventListenerService.getUpdateSelectedBehaviourInputListener(this.listBehaviours));
        save.addListener(this.eventListenerService.getEditBehaveClickListener(this));
        delete.addListener(this.getDeleteClickListener());

        //Position and Size

        this.listBehaviours.setPosition(listBehavioursX, listBehavioursY);
        startLabel.setPosition(startFieldX + 15, startFieldY + 40);
        endLabel.setPosition(endFieldX + 20, endFieldY + 40 );
        htkLabel.setPosition(hotkeyListFieldX + 20, hotkeyListFieldY + 40);
        this.startField.setPosition(startFieldX, startFieldY);
        this.endField.setPosition(endFieldX, endFieldY);
        this.hotkeyField.setPosition(hotkeyListFieldX, hotkeyListFieldY);
        this.title.setPosition(hpManaScreenTitleX, hpManaScreenTitleY);
        save.setPosition(saveX, saveY);
        delete.setPosition(deleteX, deleteY);

        this.listBehaviours.setSize(150, 50);
        this.startField.setSize(90, 40);
        this.endField.setSize(90, 40);
        this.hotkeyField.setSize(60, 40);

        //Stage add

        this.stage.addActor(this.title);
        this.stage.addActor(this.listBehaviours);
        this.stage.addActor(this.startField);
        this.stage.addActor(this.endField);
        this.stage.addActor(this.hotkeyField);
        this.stage.addActor(save);
        this.stage.addActor(delete);
        this.stage.addActor(startLabel);
        this.stage.addActor(endLabel);
        this.stage.addActor(htkLabel);
    }

    private ClickListener getDeleteClickListener(){

        return new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                try {
                    ListBehavioursScreen.this.behaviourService.removeBehaviour(ListBehavioursScreen.this.listScreenService.getSelectedBehaviour(ListBehavioursScreen.this.listBehaviours.getSelectedIndex()));
                } catch (IOException e) {
                    ListBehavioursScreen.this.errorDialogGenerator("Could not save state");
                }
                ListBehavioursScreen.this.listBehaviours.setItems(ListBehavioursScreen.this.listScreenService.getAllBehaviourNames());
                super.clicked(event, x, y);
            }
        };
    }

    public TextField getHotkeyField() {
        return this.hotkeyField;
    }

    public TextField getStartField() {
        return this.startField;
    }

    public TextField getEndField() {
        return this.endField;
    }

    public SelectBox<Object> getListBehaviours() {
        return this.listBehaviours;
    }
}
