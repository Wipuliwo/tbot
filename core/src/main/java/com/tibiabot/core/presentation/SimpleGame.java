package com.tibiabot.core.presentation;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.tibiabot.core.presentation.screens.HpManaCfgScreen;

public class SimpleGame extends Game {

    private HpManaCfgScreen featuresScreen;
    private Batch batch;


    @Override
    public void create() {
        this.batch = new SpriteBatch();
        this.featuresScreen = new HpManaCfgScreen();
        this.setScreen(this.featuresScreen);
    }

}
