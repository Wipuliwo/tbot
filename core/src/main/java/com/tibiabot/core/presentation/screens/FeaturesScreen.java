package com.tibiabot.core.presentation.screens;

import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.tibiabot.core.service.BehaviourService;
import com.tibiabot.core.service.EventListenerService;
import com.tibiabot.core.service.ScreenChangerService;
import com.tibiabot.core.state.BotState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class FeaturesScreen extends BasicScreen {

    private static final int row = 400/8;
    private static final int col = 400/8;

    @Autowired
    private BehaviourService behaviourService;

    @Autowired
    private BotState botState;

    @Autowired
    private HpManaCfgScreen healConfigScreen;

    @Autowired
    private BasicUtilitiesCfgScreen basicUtilitiesCfgScreen;

    private Button healBtn;
    private Button potBtn;
    private Button foodBtn;
    private Button ammoBtn;
    private Button paralyzeBtn;

    private Label healLabel;
    private Label potLabel;
    private Label paralyzeLabel;
    private Label ammoLabel;
    private Label foodLabel;


    @Override
    public void render(final float v) {

        super.render(v);

        //checks for the switches
        if(this.healBtn.isChecked()) {
            this.botState.setHealerIsOn(true);
        } else {
            this.botState.setHealerIsOn(false);
        }
        if(this.potBtn.isChecked()) {
            this.botState.setPotterIsOn(true);
        } else {
            this.botState.setPotterIsOn(false);
        }
        if(this.ammoBtn.isChecked()) {
            this.botState.setAmmoIsOn(true);
        } else {
            this.botState.setAmmoIsOn(false);
        }
        if(this.foodBtn.isChecked()) {
            this.botState.setEaterIsOn(true);
        } else {
            this.botState.setEaterIsOn(false);
        }
        if(this.paralyzeBtn.isChecked()) {
            this.botState.setParalyzeIsOn(true);
        } else {
            this.botState.setParalyzeIsOn(false);
        }

    }

    @Override
    public void resize(final int i, final int i1) {

    }

    @Override
    public void show() {

        super.show();

        //healer switch and label
        this.healLabel = new Label("Healer", this.skin);
        this.healLabel.addListener(this.eventListenerService.getScreenChangeClickEvent(this.healConfigScreen));
        this.healBtn = new Button(this.skin,"switch");

        this.healLabel.setPosition(20, FeaturesScreen.row *2);
        this.healBtn.setPosition(FeaturesScreen.col *2, FeaturesScreen.row *2);

        this.stage.addActor(this.healLabel);
        this.stage.addActor(this.healBtn);

        //potter switch and label
        this.potLabel = new Label("Potter", this.skin);
        this.potLabel.addListener(this.eventListenerService.getScreenChangeClickEvent(this.healConfigScreen));
        this.potBtn = new Button(this.skin,"switch");

        this.potLabel.setPosition(FeaturesScreen.col, FeaturesScreen.row *3);
        this.potBtn.setPosition(FeaturesScreen.col *3, FeaturesScreen.row *3);

        this.stage.addActor(this.potLabel);
        this.stage.addActor(this.potBtn);

        //food switch and label
        this.foodLabel = new Label("Food", this.skin);
        this.foodLabel.addListener(this.eventListenerService.getScreenChangeClickEvent(this.basicUtilitiesCfgScreen));
        this.foodBtn = new Button(this.skin,"switch");

        this.foodLabel.setPosition(2* FeaturesScreen.col, 4* FeaturesScreen.row);

        this.foodBtn.setPosition(4* FeaturesScreen.col, 4* FeaturesScreen.row);

        this.stage.addActor(this.foodLabel);
        this.stage.addActor(this.foodBtn);

        //paralyze switch and label
        this.paralyzeLabel = new Label("Paralyze", this.skin);
        this.paralyzeLabel.addListener(this.eventListenerService.getScreenChangeClickEvent(this.basicUtilitiesCfgScreen));
        this.paralyzeBtn = new Button(this.skin, "switch");

        this.paralyzeLabel.setPosition(3* FeaturesScreen.col, 5* FeaturesScreen.row);
        this.paralyzeBtn.setPosition(5* FeaturesScreen.col,5* FeaturesScreen.row);

        this.stage.addActor(this.paralyzeLabel);
        this.stage.addActor(this.paralyzeBtn);

        //ammo switch and label
        this.ammoLabel = new Label("Ammo", this.skin);
        this.ammoLabel.addListener(this.eventListenerService.getScreenChangeClickEvent(this.basicUtilitiesCfgScreen));

        this.ammoBtn = new Button(this.skin,"switch");

        this.ammoLabel.setPosition(4* FeaturesScreen.col, 6* FeaturesScreen.row);
        this.ammoBtn.setPosition(6* FeaturesScreen.col,6* FeaturesScreen.row);

        this.stage.addActor(this.ammoLabel);
        this.stage.addActor(this.ammoBtn);

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
