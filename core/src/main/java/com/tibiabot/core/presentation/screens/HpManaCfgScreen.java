package com.tibiabot.core.presentation.screens;

import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.tibiabot.core.utils.Behaviours;
import org.springframework.beans.factory.annotation.Autowired;

public class HpManaCfgScreen extends BasicScreen{

    private TextField hpName;
    private TextField manaName;
    private TextField startHp;
    private TextField endHp;
    private TextField startMana;
    private TextField endMana;
    private TextField hpHotkey;
    private TextField manaHotkey;
    private Dialog dialog;

    private TextButton list;
    private Button addHpBtn;
    private Button addManaBtn;

    private TextField foodHotkey;
    private TextField paralyzeHotkey;

    @Autowired
    private
    ListBehavioursScreen listBehavioursScreen;

    @Override
    public void show() {

        super.show();

        this.hpName = new TextField("Name", this.skin, "spinner");
        this.manaName = new TextField("name", this.skin, "spinner");
        this.startHp = new TextField("start HP", this.skin, "spinner");
        this.endHp = new TextField("end HP", this.skin, "spinner");
        this.startMana = new TextField("start MP", this.skin, "spinner");
        this.endMana = new TextField("end MP", this.skin, "spinner");
        this.hpHotkey = new TextField("HTK", this.skin, "spinner");
        this.manaHotkey = new TextField("HTK", this.skin, "spinner");
        this.list = new TextButton("List", this.skin, "default");
        this.addHpBtn = new Button(this.skin, "plus");
        this.addManaBtn = new Button(this.skin, "plus");
        this.title = new Label("Configure Healer and Potter", this.skin);

        //Listeners

        this.hpName.addListener(this.eventListenerService.getTextFieldClickListener(this.hpName));
        this.manaName.addListener(this.eventListenerService.getTextFieldClickListener(this.manaName));
        this.list.addListener(this.eventListenerService.getScreenChangeClickEvent(this.listBehavioursScreen));
        this.startHp.addListener(this.eventListenerService.getTextFieldClickListener(this.startHp));
        this.startHp.addListener(this.eventListenerService.getTextFieldInputListener(this.startHp));
        this.endHp.addListener(this.eventListenerService.getTextFieldClickListener(this.endHp));
        this.endHp.addListener(this.eventListenerService.getTextFieldInputListener(this.endHp));
        this.hpHotkey.addListener(this.eventListenerService.getHtkInputListener(this.hpHotkey));
        this.manaHotkey.addListener(this.eventListenerService.getHtkInputListener(this.manaHotkey));
        this.endMana.addListener(this.eventListenerService.getTextFieldClickListener(this.endMana));
        this.endMana.addListener(this.eventListenerService.getTextFieldInputListener(this.endMana));
        this.startMana.addListener(this.eventListenerService.getTextFieldClickListener(this.startMana));
        this.startMana.addListener(this.eventListenerService.getTextFieldInputListener(this.startMana));
        this.hpHotkey.addListener(this.eventListenerService.getTextFieldClickListener(this.hpHotkey));
        this.manaHotkey.addListener(this.eventListenerService.getTextFieldClickListener(this.manaHotkey));
        this.addHpBtn.addListener(this.eventListenerService.getAddBehaveClickEvent(this.startHp, this.endHp, this.hpHotkey, this.hpName, Behaviours.HEAL, this));
        this.addManaBtn.addListener(this.eventListenerService.getAddBehaveClickEvent(this.startMana, this.endMana, this.manaHotkey, this.manaName, Behaviours.MANA,this));

        //Positions and sizes

        this.hpName.setPosition(hpBehaveNameX, hpBehaveNameY);
        this.manaName.setPosition(manaBehaveNameX, getManaBehaveNameY);
        this.hpHotkey.setPosition(hpHotkeyX,hpHotkeyY);
        this.manaHotkey.setPosition(manaHotkeyX,manaHotkeyY);
        this.startHp.setPosition(startHpX,startHpY);
        this.endHp.setPosition(endHpX,endHpY);
        this.startMana.setPosition(startManaX,startManaY);
        this.endMana.setPosition(endManaX,endManaY);
        this.list.setPosition(hpManalistX +150, hpManalistY);
        this.addHpBtn.setPosition(addHpX, addHpY);
        this.addManaBtn.setPosition(addManaX, addManaY);
        this.title.setPosition(hpManaScreenTitleX, hpManaScreenTitleY);

        this.hpHotkey.setWidth(50);
        this.manaHotkey.setWidth(50);
        this.list.setWidth(this.list.getWidth() + 50);

        this.endMana.setWidth(80);
        this.endHp.setWidth(80);
        this.startMana.setWidth(80);
        this.startHp.setWidth(80);

        //Stage add

        this.stage.addActor(this.hpName);
        this.stage.addActor(this.manaName);
        this.stage.addActor(this.startMana);
        this.stage.addActor(this.startHp);
        this.stage.addActor(this.endHp);
        this.stage.addActor(this.endMana);
        this.stage.addActor(this.manaHotkey);
        this.stage.addActor(this.hpHotkey);
        this.stage.addActor(this.list);
        this.stage.addActor(this.addHpBtn);
        this.stage.addActor(this.addManaBtn);
        this.stage.addActor(this.title);
    }
}
