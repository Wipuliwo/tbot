package com.tibiabot.core.utils;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.tibiabot.core.exception.BadInputException;

import java.awt.event.KeyEvent;
import java.util.Arrays;

public class TbotUtils {

    private static String hotkeyErrosMsg = "Could not map hotkey";

    static public void resetTextField(TextField... fields){

        Arrays.stream(fields).forEach( field -> field.setText(""));

    }

    static public int gdxKeysToKeyEvent(int gdxKeysKeyCode) throws BadInputException {

        if(gdxKeysKeyCode < 0) {
            throw new BadInputException(hotkeyErrosMsg);
        }
        try {
            return (int) KeyEvent.class.getField("VK_"+ Input.Keys.toString(gdxKeysKeyCode)).get(gdxKeysKeyCode);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new BadInputException(hotkeyErrosMsg);
        }

    }

    static public int gdxKeysToKeyEvent(String gdxKeyString) throws BadInputException{

        if(gdxKeyString.isEmpty()){
            throw new BadInputException(hotkeyErrosMsg);
        }
        try {
            return (int) KeyEvent.class.getField("VK_"+ gdxKeyString).get(gdxKeyString);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new BadInputException(hotkeyErrosMsg);
        }
    }
}
