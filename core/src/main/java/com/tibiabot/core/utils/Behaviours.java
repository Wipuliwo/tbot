package com.tibiabot.core.utils;

public enum Behaviours {
    HEAL,
    MANA,
    EATER,
    AMMO,
    PARALYZE
}
