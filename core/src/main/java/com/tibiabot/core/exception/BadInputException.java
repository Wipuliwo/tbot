package com.tibiabot.core.exception;

public class BadInputException extends RuntimeException {

    private static final long serialVersionUID = 4430657103864160202L;

    public BadInputException(String message) {
        super(message);
    }
}
