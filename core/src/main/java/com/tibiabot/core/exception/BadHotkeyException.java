package com.tibiabot.core.exception;

public class BadHotkeyException extends Exception {

    private static final long serialVersionUID = 8748663606717934542L;

    public BadHotkeyException() {
        super("Cannot map hotkey");
    }
}
