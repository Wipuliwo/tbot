package com.tibiabot.core.exception;

public class BehaviourExists extends Exception {

    private static final long serialVersionUID = 8748663606717934542L;

    public BehaviourExists() {
        super("There is already a behaviour within that range.\nCheck list screen.");
    }

}
