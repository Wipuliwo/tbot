package com.tibiabot.core.engine.paralize;

import com.tibiabot.core.engine.common.SimpleBehaviour;
import com.tibiabot.core.engine.common.abstractions.Abehaviour;
import com.tibiabot.core.engine.common.abstractions.Aworker;
import com.tibiabot.core.engine.common.resources.BotConfig;

import java.util.List;

public class ParalyzeEngine extends Aworker<SimpleBehaviour>{

    private final int checkTimes = 80;


    @Override
    public void work(final List<SimpleBehaviour> abehaviours) {
        int i = 0;
        int color;

        while(i< this.checkTimes){
            color = this.getRobot().getPixelColor(BotConfig.paralizeStartX + i,BotConfig.paralizeSartY).getRGB();
            for (final Abehaviour s : abehaviours){
                if(color == s.getColor()){
                    synchronized (this.getTibiaInputer()){
                        this.getTibiaInputer().cast(s);
                    }
                    try {
                        Thread.sleep(750);
                    } catch (final InterruptedException e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }
            i=i+10;
        }
    }
}
