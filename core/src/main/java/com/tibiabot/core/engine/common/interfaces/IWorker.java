package com.tibiabot.core.engine.common.interfaces;

import com.tibiabot.core.engine.common.abstractions.Abehaviour;

import java.util.List;

public interface IWorker<T extends Abehaviour> {

    void work(List<T> behaviours);

}
