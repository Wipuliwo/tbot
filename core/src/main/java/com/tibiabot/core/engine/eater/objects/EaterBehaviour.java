package com.tibiabot.core.engine.eater.objects;

import com.tibiabot.core.engine.common.abstractions.Abehaviour;

public class EaterBehaviour extends Abehaviour {

    private static final long serialVersionUID = 4818127720437556151L;

    public int getTimer() {
        return this.timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    private int timer;


}
