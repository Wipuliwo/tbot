package com.tibiabot.core.engine.common.abstractions;

import com.tibiabot.core.utils.Behaviours;

import java.io.Serializable;

public abstract class Abehaviour implements Serializable {

    private static final long serialVersionUID = -3516889016394723400L;
    private String name = "Default";

    private int startX;
    private int startY;
    private int hotkey;
    private int color;

    private String startValue = "N/A" ;
    private String endValue = "N/A";

    private Behaviours type;

    public Behaviours getType() {
        return this.type;
    }

    public void setType(Behaviours type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }
    public int getStartX() {
        return this.startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return this.startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public int getHotkey() {
        return this.hotkey;
    }

    public void setHotkey(int hotkey) {
        this.hotkey = hotkey;
    }

    public int getColor() {
        return this.color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartValue() {
        return this.startValue;
    }

    public void setStartValue(String startValue) {
        this.startValue = startValue;
    }

    public String getEndValue() {
        return this.endValue;
    }

    public void setEndValue(String endValue) {
        this.endValue = endValue;
    }

    @Override
    public String toString() {
        return "Abehaviour{" +
                "startX=" + this.startX +
                ", startY=" + this.startY +
                ", hotkey=" + this.hotkey +
                ", color=" + this.color +
                '}';
    }
}
