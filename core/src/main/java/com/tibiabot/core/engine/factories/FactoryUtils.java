package com.tibiabot.core.engine.factories;


import com.tibiabot.core.engine.common.resources.BotConfig;

public class FactoryUtils {

    protected static int manaHpPercentValue(int value){

        float pixels = BotConfig.hpBarEndX - BotConfig.hpBarStartX;
        pixels = pixels/100;

        return  Math.round(pixels*value);

    }
}
