package com.tibiabot.core.engine.factories;

import com.tibiabot.core.engine.ammo.AmmoBehaviour;
import com.tibiabot.core.engine.common.SimpleBehaviour;
import com.tibiabot.core.engine.common.resources.BotConfig;
import com.tibiabot.core.engine.eater.objects.EaterBehaviour;
import com.tibiabot.core.engine.healer.objects.HealBehaviour;
import com.tibiabot.core.utils.Behaviours;

public class Factory {

    private static FactoryUtils factoryUtils;

    public Factory(){
        factoryUtils = new FactoryUtils();
    }

    public static HealBehaviour createHpBehaviour(final int start, final int end, final int hotkey, String name){

        final HealBehaviour simple = new HealBehaviour();

        simple.setName(name);
        simple.setStartValue(String.valueOf(start));
        if(end >= 0) {
            simple.setEndValue(String.valueOf(end));
        } else {
            simple.setEndValue("0");
        }

        simple.setColor(BotConfig.hpBackground);
        simple.setHotkey(hotkey);
        simple.setStartX(BotConfig.hpBarStartX + factoryUtils.manaHpPercentValue(start));
        simple.setStartY(BotConfig.hpBarY);
        simple.setType(Behaviours.HEAL);
        if(end < 0){

            simple.setEndX(-1);
            simple.setEndY(-1);

        }
        else {
            simple.setEndX(BotConfig.hpBarStartX + factoryUtils.manaHpPercentValue(end));
            simple.setEndY(BotConfig.hpBarY);
        }

        return simple;
    }

    public static HealBehaviour createManaBehaviour(final int start, final int end, final int hotkey, String name){

        final HealBehaviour simple = new HealBehaviour();

        simple.setName(name);
        simple.setStartValue(String.valueOf(start));
        if(end>=0) {
            simple.setEndValue(String.valueOf(end));
        } else {
            simple.setEndValue("0");
        }

        simple.setColor(BotConfig.manaBackground);
        simple.setHotkey(hotkey);
        simple.setStartX(BotConfig.manaBarStartX + factoryUtils.manaHpPercentValue(start));
        simple.setStartY(BotConfig.manaBarY);
        simple.setType(Behaviours.MANA);
        if(end < 0){

            simple.setEndX(-1);
            simple.setEndY(-1);

        }
        else{

            simple.setEndX(BotConfig.manaBarStartX + factoryUtils.manaHpPercentValue(end));
            simple.setEndY(BotConfig.manaBarY);
        }

        return simple;
    }

    public static EaterBehaviour createEaterBehaviour(final int timer, final int hotkey,  String name){

        final EaterBehaviour b = new EaterBehaviour();

        b.setType(Behaviours.EATER);
        b.setName(name);
        b.setTimer(timer);
        b.setStartY(-1);
        b.setStartX(-1);
        b.setHotkey(hotkey);
        b.setColor(-1);

        return b;
    }

    public static AmmoBehaviour createAmmoBehaviour(final int hotkey,  String name){

        final AmmoBehaviour ammoBehaviour = new AmmoBehaviour();

        ammoBehaviour.setType(Behaviours.AMMO);
        ammoBehaviour.setName(name);
        ammoBehaviour.setColor(BotConfig.ammoColor);
        ammoBehaviour.setHotkey(hotkey);
        ammoBehaviour.setStartX(BotConfig.ammoX);
        ammoBehaviour.setStartY(BotConfig.ammoY);

        return ammoBehaviour;

    }

    public static SimpleBehaviour createParalizeBehaviour(final int hotkey,  String name){

        final SimpleBehaviour utilSpellBehaviour = new SimpleBehaviour();

        utilSpellBehaviour.setType(Behaviours.PARALYZE);
        utilSpellBehaviour.setName(name);
        utilSpellBehaviour.setColor(BotConfig.paralizeColor);
        utilSpellBehaviour.setStartX(BotConfig.paralizeStartX);
        utilSpellBehaviour.setStartY(BotConfig.paralizeSartY);
        utilSpellBehaviour.setHotkey(hotkey);

        return utilSpellBehaviour;
    }

}
