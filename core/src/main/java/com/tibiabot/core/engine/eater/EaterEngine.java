package com.tibiabot.core.engine.eater;

import com.tibiabot.core.engine.common.abstractions.Abehaviour;
import com.tibiabot.core.engine.common.abstractions.Aworker;
import com.tibiabot.core.engine.eater.objects.EaterBehaviour;

import java.util.List;


public class EaterEngine extends Aworker<EaterBehaviour> {

    @Override
    public void work(final List<EaterBehaviour> behaviours) {

            for (final Abehaviour o : behaviours) {
                synchronized (this.getTibiaInputer()) {
                this.getTibiaInputer().cast(o);
                break;
            }

        }
    }
}
