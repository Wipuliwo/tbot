package com.tibiabot.core.engine.healer.objects;


import com.tibiabot.core.engine.common.abstractions.Abehaviour;

public class HealBehaviour extends Abehaviour{


    private int endX;
    private int endY;

    public int getEndX() {
        return this.endX;
    }

    public void setEndX(int endX) {
        this.endX = endX;
    }

    public int getEndY() {
        return this.endY;
    }

    public void setEndY(int endY) {
        this.endY = endY;
    }

    @Override
    public String toString() {
        return "HealBehaviour{" +
                "endX=" + this.endX +
                ", endY=" + this.endY +
                "} " + super.toString();
    }
}


