package com.tibiabot.core.engine.common.abstractions;

import com.tibiabot.core.engine.common.interfaces.Ichecker;
import org.springframework.beans.factory.annotation.Autowired;


import java.awt.*;

public abstract class Achecker<T extends Abehaviour> implements Ichecker<T> {

    @Autowired
    private Robot robot;

    protected Robot getRobot() {

        return this.robot;
    }

    @Override
    public boolean check(final T behaviour) {

        return this.robot.getPixelColor(behaviour.getStartX(), behaviour.getStartY()).getRGB() == behaviour.getColor();

    }

    public void setRobot(final Robot robot) {
        this.robot = robot;

    }


}
