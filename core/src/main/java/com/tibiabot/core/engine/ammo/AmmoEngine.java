package com.tibiabot.core.engine.ammo;

import com.tibiabot.core.engine.common.abstractions.Abehaviour;
import com.tibiabot.core.engine.common.abstractions.Aworker;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AmmoEngine extends Aworker<AmmoBehaviour> {


    @Override
    public void work(final List<AmmoBehaviour> ammoBehaviours) {

        for(final Abehaviour c : ammoBehaviours ){

            if(this.getRobot().getPixelColor(c.getStartX(),c.getStartY()).getRGB() == c.getColor()){

                synchronized (this.getTibiaInputer()) {
                    this.getTibiaInputer().cast(c);
                }
                break;

            }

        }

    }
}
