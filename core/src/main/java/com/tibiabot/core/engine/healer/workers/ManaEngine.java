package com.tibiabot.core.engine.healer.workers;

import com.tibiabot.core.engine.common.abstractions.Aworker;
import com.tibiabot.core.engine.healer.objects.HealBehaviour;
import com.tibiabot.core.engine.paralize.ParalyzeEngine;
import com.tibiabot.core.service.BehaviourService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ManaEngine extends Aworker<HealBehaviour> {

    @Autowired
    private HpMChecker checker;

    @Autowired
    private ParalyzeEngine paralyzeEngine;

    @Autowired
    private
    BehaviourService behaviourService;

    public ManaEngine(){
        super();
    }

    @Override
    public void work(final List<HealBehaviour> abehaviourList)  {

        for (final HealBehaviour s : abehaviourList) {
            if (!this.checker.check(s)) {
                synchronized (this.getTibiaInputer()) {
                    this.getTibiaInputer().cast(s);
                }
                try {
                    Thread.sleep(750);
                } catch (final InterruptedException e) {
                    e.printStackTrace();
                }
                return;
            }
        }
        this.paralyzeEngine.work(this.behaviourService.getParalyzeBehaviours());
    }}
