package com.tibiabot.core.engine.common.abstractions;

import com.tibiabot.core.engine.common.TibiaInputer;
import com.tibiabot.core.engine.common.interfaces.IWorker;
import org.springframework.beans.factory.annotation.Autowired;


import java.awt.*;

public abstract class Aworker<T extends Abehaviour> implements IWorker<T> {

    @Autowired
    private Robot robot;
    @Autowired
    private TibiaInputer tibiaInputer;



    //Getters and setters
    protected Robot getRobot() { return this.robot; }
    public void setRobot(final Robot robot) { this.robot = robot; }
    protected TibiaInputer getTibiaInputer() { return this.tibiaInputer; }
    public void setTibiaInputer(final TibiaInputer tibiaInputer) { this.tibiaInputer = tibiaInputer; }




}
