package com.tibiabot.core.engine.healer.workers;

import com.tibiabot.core.engine.common.abstractions.Achecker;
import com.tibiabot.core.engine.healer.objects.HealBehaviour;

public class HpMChecker extends Achecker<HealBehaviour> {


    @Override
    public boolean check(final HealBehaviour h) {
        if(h.getEndX() != -1 ){


            return this.getRobot().getPixelColor(h.getEndX(), h.getEndY()).getRGB() == h.getColor() ?
                    (this.getRobot().getPixelColor(h.getStartX(), h.getStartY()).getRGB() == h.getColor()): true;

        }

        return super.check(h);

    }
}
