package com.tibiabot.core.logic;

import com.tibiabot.core.engine.common.abstractions.Abehaviour;

import java.util.List;

public class BehaviourBO {

    public Boolean newIsAllowed(Abehaviour behaviour, List<Abehaviour> behaviourList){

        Integer startValue = Integer.valueOf(behaviour.getStartValue());
        Integer endValue = Integer.valueOf(behaviour.getEndValue());

        return behaviourList.stream().noneMatch(b -> (startValue <= Integer.valueOf(b.getStartValue()) && startValue <= Integer.valueOf(b.getEndValue()))
                || (endValue <= Integer.valueOf(b.getStartValue()) && endValue >= Integer.valueOf(b.getEndValue())));
    }

}
