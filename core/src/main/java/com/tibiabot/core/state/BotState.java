package com.tibiabot.core.state;

import org.springframework.stereotype.Component;

@Component
public class BotState implements  IBotState {

    private Boolean botIsOn;

    private LicenceState licenceState;

    private Boolean eaterIsOn;
    private Boolean healerIsOn;
    private Boolean potterIsOn;
    private Boolean ammoIsOn;
    private Boolean paralyzeIsOn;

    public BotState(){

        this.eaterIsOn =false;
        this.botIsOn = false;
        this.potterIsOn = false;
        this.ammoIsOn = false;
        this.paralyzeIsOn = false;
        this.healerIsOn = false;
    }

    public LicenceState getLicenceState() {
        return this.licenceState;
    }

    public void setLicenceState(final LicenceState licenceState) {
        this.licenceState = licenceState;
    }

    public void setBotIsOn(final Boolean botIsOn) {
        this.botIsOn = botIsOn;
    }

    public void setEaterIsOn(final Boolean eaterIsOn) {
        this.eaterIsOn = eaterIsOn;
    }

    public void setHealerIsOn(final Boolean healerIsOn) {
        this.healerIsOn = healerIsOn;
    }

    public void setPotterIsOn(final Boolean potterIsOn) {
        this.potterIsOn = potterIsOn;
    }

    public void setAmmoIsOn(final Boolean ammoIsOn) {
        this.ammoIsOn = ammoIsOn;
    }

    public void setParalyzeIsOn(final Boolean paralyzeIsOn) {
        this.paralyzeIsOn = paralyzeIsOn;
    }

    @Override
    public Boolean botIsOn() {
        return this.botIsOn;
    }

    @Override
    public Boolean healerIsOn() {
        return this.healerIsOn;
    }

    @Override
    public Boolean ammoIsOn() {
        return this.ammoIsOn;
    }

    @Override
    public Boolean paralyzeIsOn() {
        return this.paralyzeIsOn;
    }

    @Override
    public Boolean eaterIsOn() {
        return this.eaterIsOn;
    }

    @Override
    public Boolean potterIsOn() {
        return this.potterIsOn;
    }

    @Override
    public LicenceState getLicenseState() {
        return null;
    }


}
