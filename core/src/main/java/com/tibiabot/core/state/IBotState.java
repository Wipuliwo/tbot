package com.tibiabot.core.state;

public interface IBotState {

    Boolean botIsOn();

    Boolean healerIsOn();
    Boolean ammoIsOn();
    Boolean paralyzeIsOn();
    Boolean eaterIsOn();
    Boolean potterIsOn();

    LicenceState getLicenseState();

}
