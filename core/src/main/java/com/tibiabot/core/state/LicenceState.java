package com.tibiabot.core.state;

public enum LicenceState {

    EXPIRED, BASIC, FULL;

}
