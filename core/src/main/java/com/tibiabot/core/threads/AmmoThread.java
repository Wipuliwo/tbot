package com.tibiabot.core.threads;

import com.tibiabot.core.engine.ammo.AmmoEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AmmoThread extends AbsThread implements Runnable {

    @Autowired
    private
    AmmoEngine ammoEngine;


    @Override
    public void run() {

        while(true) {
            if (this.isOn()) {
                try {
                    Thread.sleep(750);
                    this.ammoEngine.work(this.behaviourService.getAmmoBehaviours());
                } catch (final InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                synchronized (this){
                    try {
                        this.wait();
                    } catch (final InterruptedException e) {
                        e.printStackTrace();
                    }
                }}
        }
    }
}
