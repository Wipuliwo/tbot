package com.tibiabot.core.threads;

import com.tibiabot.core.engine.common.abstractions.Aworker;
import com.tibiabot.core.service.BehaviourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public abstract class AbsThread<T extends Aworker>  {

    @Autowired
    BehaviourService behaviourService;

    private boolean isOn = false;

    AbsThread() {
    }

    boolean isOn() {
        return this.isOn;
    }

    public void setOn(final boolean on) {
        this.isOn = on;
    }

    public BehaviourService getBehaviourService() {
        return this.behaviourService;
    }
}
