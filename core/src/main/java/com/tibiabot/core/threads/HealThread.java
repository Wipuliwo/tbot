package com.tibiabot.core.threads;

import com.tibiabot.core.engine.healer.workers.HealerEngine;
import com.tibiabot.core.service.BehaviourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HealThread extends AbsThread<HealerEngine> implements Runnable {


    @Autowired
    private
    HealerEngine healerEngine;

    @Autowired
    private
    BehaviourService behaviourService;

    @Override
    public void run() {

     while(true) {
         if (this.isOn()) {
             this.healerEngine.work(this.behaviourService.getHpBehaviours());
         } else {
             synchronized (this){
             try {
                 this.wait();
             } catch (final InterruptedException e) {
                 e.printStackTrace();
             }
         }}
     }
    }

}
