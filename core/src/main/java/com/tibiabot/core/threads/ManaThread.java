package com.tibiabot.core.threads;

import com.tibiabot.core.engine.healer.workers.ManaEngine;
import com.tibiabot.core.service.BehaviourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManaThread extends AbsThread<ManaEngine> implements Runnable  {

    @Autowired
    private
    ManaEngine healerEngine;

    @Autowired
    private
    BehaviourService behaviourService;

    @Override
    public void run() {

        while(true) {
            if (this.isOn()) {
                this.healerEngine.work(this.behaviourService.getManaBehaviours());
            } else {
                synchronized (this){
                    try {
                        this.wait();
                    } catch (final InterruptedException e) {
                        e.printStackTrace();
                    }
                }}
        }
    }
}
