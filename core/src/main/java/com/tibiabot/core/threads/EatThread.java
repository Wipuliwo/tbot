package com.tibiabot.core.threads;

import com.tibiabot.core.engine.eater.EaterEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EatThread extends AbsThread<EaterEngine> implements Runnable {

    private ThreadPool threadPool;

    @Autowired
    private
    EaterEngine eaterEngine;


    @Override
    public void run() {

        while(true) {
            try {
                if (this.isOn()) {
                        Thread.sleep(60000);
                    this.eaterEngine.work(this.behaviourService.getEatBehaviours());
                }else {
                    synchronized (this) {
                        this.wait();
                    }
                }
                }   catch (final InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
