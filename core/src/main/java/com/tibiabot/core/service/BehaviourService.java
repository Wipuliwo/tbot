package com.tibiabot.core.service;

import com.tibiabot.core.exception.BadHotkeyException;
import com.tibiabot.core.exception.BadInputException;
import com.tibiabot.core.exception.BehaviourExists;
import com.tibiabot.core.engine.ammo.AmmoBehaviour;
import com.tibiabot.core.engine.common.SimpleBehaviour;
import com.tibiabot.core.engine.common.abstractions.Abehaviour;
import com.tibiabot.core.engine.eater.objects.EaterBehaviour;
import com.tibiabot.core.engine.factories.Factory;
import com.tibiabot.core.engine.healer.objects.HealBehaviour;
import com.tibiabot.core.logic.BehaviourBO;
import com.tibiabot.core.utils.Behaviours;
import com.tibiabot.core.utils.TbotUtils;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class BehaviourService {

    @Autowired
    private
    SaveLoadService saveLoadService;

    @Autowired
    private
    BehaviourBO behaviourBO;

    private final List<HealBehaviour> hpBehaviours = new ArrayList<HealBehaviour>();
    private final List<HealBehaviour> manaBehaviours = new ArrayList<HealBehaviour>();
    private final List<EaterBehaviour> eatBehaviours = new ArrayList<EaterBehaviour>();
    private final List<AmmoBehaviour> ammoBehaviours = new ArrayList<AmmoBehaviour>();
    private final List<SimpleBehaviour> paralyzeBehaviours = new ArrayList<SimpleBehaviour>();
    private Abehaviour selectedBehaviour;

    public void load() throws IOException, ClassNotFoundException {
        this.saveLoadService.loadBehaves(this.hpBehaviours, this.manaBehaviours, this.eatBehaviours, this.ammoBehaviours, this.paralyzeBehaviours);
    }

    void save() throws IOException {
        this.saveLoadService.saveBehaves(this.getAllBehaves());
    }

    public List<HealBehaviour> getHpBehaviours() {
        return this.hpBehaviours;
    }

    public List<HealBehaviour> getManaBehaviours() {
        return this.manaBehaviours;
    }

    public List<EaterBehaviour> getEatBehaviours() {
        return this.eatBehaviours;
    }

    public List<AmmoBehaviour> getAmmoBehaviours() {
        return this.ammoBehaviours;
    }

    public List<SimpleBehaviour> getParalyzeBehaviours() {
        return this.paralyzeBehaviours;
    }

    List<Abehaviour> getAllBehaves(){

        List<Abehaviour> behaves = new ArrayList<>();
        behaves.addAll(this.eatBehaviours);
        behaves.addAll(this.ammoBehaviours);
        behaves.addAll(this.hpBehaviours);
        behaves.addAll(this.manaBehaviours);
        behaves.addAll(this.paralyzeBehaviours);

        return behaves;

    }

    public boolean removeBehaviour(Abehaviour abehaviour) throws IOException{

        if(!this.hpBehaviours.remove(abehaviour)) {
            if(!this.manaBehaviours.remove(abehaviour)) {
                if (!this.eatBehaviours.remove(abehaviour)) {
                    if (!this.ammoBehaviours.remove(abehaviour)) {
                        if(!this.paralyzeBehaviours.remove(abehaviour)) {
                            return false;
                        }
                    }
                }
            }
        }
        this.save();
        return true;
    }

    void insertNewBehave(String name, String start, String end, Integer htk, Behaviours type) throws BehaviourExists, BadInputException, IOException {

        List<Abehaviour> allbehaves = this.getAllBehaves();

        if(this.getAllBehaves().stream().noneMatch(b -> b.getHotkey() == htk)){

            int hotkey = TbotUtils.gdxKeysToKeyEvent(htk);
            String behaviourExistsMessage = "There is already a behaviour in that range.\nCheck list screen";

            switch (type){

                case HEAL:
                    HealBehaviour hb = Factory.createHpBehaviour(Integer.valueOf(start), Integer.valueOf(end), hotkey, name);
                    if(this.behaviourBO.newIsAllowed(hb, allbehaves)){
                        this.getHpBehaviours().add(Factory.createHpBehaviour(Integer.valueOf(start), Integer.valueOf(end), htk, name));
                        return;
                    }
                        throw new BehaviourExists();

                case MANA:
                    HealBehaviour mb = Factory.createManaBehaviour(Integer.valueOf(start), Integer.valueOf(end), hotkey, name);
                    if(this.behaviourBO.newIsAllowed(mb, allbehaves)) {

                        this.getManaBehaviours().add(Factory.createManaBehaviour(Integer.valueOf(start), Integer.valueOf(end), htk, name));
                        break;
                    }
                        throw new BehaviourExists();

                case EATER:
                    this.getEatBehaviours().add(Factory.createEaterBehaviour(2500, htk, name));
                            break;

                case AMMO:
                    this.getAmmoBehaviours().add(Factory.createAmmoBehaviour(htk, name));
                            break;

                case PARALYZE:
                    this.getParalyzeBehaviours().add(Factory.createParalizeBehaviour(htk, name));
                                break;
                default: throw new BehaviourExists();
            }
            this.save();
        }
    }

    void setSelectedBehaviour(int selectedIndex) {
        this.selectedBehaviour = this.getAllBehaves().get(selectedIndex);
    }
}
