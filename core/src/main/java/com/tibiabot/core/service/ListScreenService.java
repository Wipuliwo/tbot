package com.tibiabot.core.service;

import com.tibiabot.core.engine.common.abstractions.Abehaviour;
import com.tibiabot.core.engine.healer.objects.HealBehaviour;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListScreenService {

    @Autowired
    private
    BehaviourService behaviourService;

    private Abehaviour selectedBehaviour;

    public String[] getAllBehaviourNames(){
        return  this.behaviourService.getAllBehaves().stream().map(Abehaviour::getName).toArray(String[]::new);
    }

    public Abehaviour getSelectedBehaviour(int index){

        List<Abehaviour> list = this.behaviourService.getAllBehaves();

        if(!list.isEmpty()){
            return list.get(index);
        }else{
            return new HealBehaviour();
        }  }
}