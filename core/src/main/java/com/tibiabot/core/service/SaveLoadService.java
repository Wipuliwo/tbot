package com.tibiabot.core.service;

import com.tibiabot.core.engine.ammo.AmmoBehaviour;
import com.tibiabot.core.engine.common.SimpleBehaviour;
import com.tibiabot.core.engine.common.abstractions.Abehaviour;
import com.tibiabot.core.engine.eater.objects.EaterBehaviour;
import com.tibiabot.core.engine.healer.objects.HealBehaviour;
import com.tibiabot.core.utils.Behaviours;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;

@Service
public class SaveLoadService  {


    static void saveBehaves(List<Abehaviour> behaviours) throws IOException {

    File saveFile = new File("C:\\Users\\Wipul\\Desktop\\workspace\\projects\\tbot\\yourfile.txt");
    if(saveFile.exists()) {
        saveFile.delete();
    }

    FileOutputStream fileOutputStream
            = new FileOutputStream("yourfile.txt");
    ObjectOutputStream objectOutputStream
            = new ObjectOutputStream(fileOutputStream);
    objectOutputStream.writeObject(behaviours);
    objectOutputStream.flush();
    objectOutputStream.close();

    }

    void loadBehaves(List<HealBehaviour> healBehaves, List<HealBehaviour> manaBehaves, List<EaterBehaviour> eaterBehaves,
                     List<AmmoBehaviour> ammoBehaves, List<SimpleBehaviour> paralyzeBehaves) throws IOException, ClassNotFoundException {

        FileInputStream fileInputStream = new FileInputStream("yourfile.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        List<Abehaviour> behaves = (List<Abehaviour>) objectInputStream.readObject();
        objectInputStream.close();

        behaves.stream().forEach( b -> {
            if(b.getType() == Behaviours.HEAL){
                healBehaves.add((HealBehaviour) b);
            }else if(b.getType() == Behaviours.MANA){
                manaBehaves.add((HealBehaviour) b);
            }else if(b.getType() == Behaviours.EATER){
                eaterBehaves.add((EaterBehaviour) b);
            }else if(b.getType() == Behaviours.AMMO){
                ammoBehaves.add((AmmoBehaviour) b);
            }else if(b.getType() == Behaviours.PARALYZE){
                paralyzeBehaves.add((SimpleBehaviour) b);
            }
        });
    }
}