package com.tibiabot.core.service;

import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.nio.charset.Charset;
import java.util.Base64;

@Service
class LoginService {

    boolean logIn(String username, String password) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders basicAuthHeaders = this.createBasicAuthHeaders(username, password);
        try {
            ResponseEntity<String> jwt = restTemplate.exchange("http://localhost:8082/auth", HttpMethod.GET, new HttpEntity<>(basicAuthHeaders), String.class);
            HttpHeaders jwtAuthHeaders = this.createJwtAuthHeaders(jwt.getBody());
            ResponseEntity<Boolean> responseObj = restTemplate.exchange("http://localhost:8082/api/tbot", HttpMethod.GET, new HttpEntity<>(jwtAuthHeaders), Boolean.class);
            return responseObj.getBody();
        }catch (RestClientException ex){
            return false;
        }
    }

    private HttpHeaders createBasicAuthHeaders(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            this.set( "Authorization", authHeader ); }};
    }

    private HttpHeaders createJwtAuthHeaders(String jwt){
        return new HttpHeaders(){{
            this.set("Authorization", "Bearer " +  jwt);
        }};
    }
}
