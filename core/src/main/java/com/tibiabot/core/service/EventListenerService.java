package com.tibiabot.core.service;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.tibiabot.core.engine.common.abstractions.Abehaviour;
import com.tibiabot.core.exception.BadHotkeyException;
import com.tibiabot.core.exception.BadInputException;
import com.tibiabot.core.exception.BehaviourExists;
import com.tibiabot.core.presentation.screens.BasicScreen;
import com.tibiabot.core.presentation.screens.ListBehavioursScreen;
import com.tibiabot.core.utils.Behaviours;
import com.tibiabot.core.utils.TbotUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EventListenerService {

    @Autowired
    private
    ScreenChangerService screenChangerService;

    @Autowired
    private BehaviourService behaviourService;

    @Autowired
    private
    ListScreenService listScreenService;

    @Autowired
    private
    LoginService loginService;

    public ClickListener getGobackClickEvent(){

        return new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                EventListenerService.this.screenChangerService.goBack();
            }
        };
    }

    public ClickListener getLoginClickEvent(final Screen screen, BasicScreen basicScreen, TextField userField, TextField passwordField){
        return new ClickListener(){
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                if(EventListenerService.this.loginService.logIn(userField.getText(), passwordField.getText())) {
                    EventListenerService.this.screenChangerService.changeScreen(screen);
                }else{
                    basicScreen.errorDialogGenerator("No valid Licence");
                }
            }};
    }


    public ClickListener getScreenChangeClickEvent(final Screen screen){

        return new ClickListener(){
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                EventListenerService.this.screenChangerService.changeScreen(screen);
            }};
    }

    public InputListener getHtkInputListener(TextField textField){

        return new InputListener(){
            @Override
            public boolean keyUp(InputEvent event, int keycode) {

               textField.setText(Input.Keys.toString(keycode));
                return super.keyUp(event, keycode);
            }
        };
    }

    public ClickListener getTextFieldClickListener(TextField textField){

        return new ClickListener(){

            Integer counter = 0;

            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                if(this.counter == 0){
                    this.counter++;
                    textField.setText("");
                }
            }};
    }

    public InputListener getTextFieldInputListener(TextField textField){
        return new InputListener(){
            @Override
            public boolean keyTyped(InputEvent event, char character) {
                String text = textField.getText();
                if(Character.toString(character).matches("[0-9]")) {
                    if(Integer.valueOf(text) > 100) {
                        textField.setText("100");
                    }
                }else{
                    textField.setText(text.replace(Character.toString(character), ""));
                }
                return super.keyTyped(event, character);
            }
        };
    }

    public ClickListener getAddBehaveClickEvent(TextField start, TextField end, TextField htk, TextField name, Behaviours behaviourType, BasicScreen basicScreen){

        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                String startValue = null;
                String endValue = null;
                if(behaviourType == Behaviours.HEAL || behaviourType.equals(Behaviours.MANA)) {
                    if (name.getText().isEmpty() || start.getText().isEmpty() || start.getText().contains("[a-zA-Z]") || end.getText().isEmpty() || end.getText().contains("[a-zA-Z]") || htk.getText().isEmpty() || htk.getText().equals("HTK")) {
                        basicScreen.errorDialogGenerator("Incorrect field Input\nWrong data type or empty");
                        return;
                    }
                    if (Integer.valueOf(end.getText()) >= Integer.valueOf(start.getText())) {
                        basicScreen.errorDialogGenerator("Incorrect field input\nEnd Value bigger or equal to start value");
                        return;
                    }
                    startValue = start.getText();
                    endValue = end.getText();
                }
                try {
                    EventListenerService.this.behaviourService.insertNewBehave(name.getText(), startValue, endValue, TbotUtils.gdxKeysToKeyEvent(htk.getText()), behaviourType);
                    EventListenerService.this.behaviourService.save();
                } catch (BehaviourExists | BadInputException ex) {
                    basicScreen.errorDialogGenerator(ex.getMessage());
                    return;
                } catch(IOException ex){
                    basicScreen.errorDialogGenerator("Could not save behaviour to file");
                }
                basicScreen.dispose();
                    basicScreen.show();
                    super.clicked(event, x, y);
            }
        };
    }

    public ClickListener getEditBehaveClickListener(ListBehavioursScreen behavioursScreen){
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                int index = behavioursScreen.getListBehaviours().getSelectedIndex();
                Abehaviour behaviourToEdit = EventListenerService.this.listScreenService.getSelectedBehaviour(index);

                String name = behaviourToEdit.getName();
                String start = behavioursScreen.getStartField().getText();
                String end =   behavioursScreen.getEndField().getText();
                try {

                    Integer htk = TbotUtils.gdxKeysToKeyEvent(Input.Keys.valueOf(behavioursScreen.getHotkeyField().getText()));
                    EventListenerService.this.behaviourService.removeBehaviour(behaviourToEdit);
                    EventListenerService.this.behaviourService.insertNewBehave(name, start, end, htk, behaviourToEdit.getType());
                } catch(BehaviourExists e ){
                    behavioursScreen.errorDialogGenerator(e.getMessage());
                } catch(IOException e){
                    behavioursScreen.errorDialogGenerator("Error.\nCould not find behaviour");
                }
                super.clicked(event, x, y);
            }
        };
    }

    public EventListener getUpdateSelectedBehaviourInputListener(SelectBox<Object> listBehaviours) {
        return new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EventListenerService.this.behaviourService.setSelectedBehaviour(listBehaviours.getSelectedIndex());
                super.clicked(event, x, y);
            }
        };
    }
}
