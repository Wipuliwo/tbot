package com.tibiabot.core.service;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.tibiabot.core.presentation.screens.FeaturesScreen;
import com.tibiabot.core.presentation.screens.MenuScreen;
import com.tibiabot.core.state.BotState;
import org.springframework.beans.factory.annotation.Autowired;

public class ScreenChangerService {

    @Autowired
    private
    Game game;

    @Autowired
    private
    FeaturesScreen featuresScreen;

    @Autowired
    private
    MenuScreen menuScreen;

    @Autowired
    private BotState botState;

    void changeScreen(final Screen screen){
        this.game.setScreen(screen);
    }

    public void goBack(){

        if(this.game.getScreen() == this.featuresScreen) {

            //stop the threads
            this.botState.setParalyzeIsOn(false);
            this.botState.setAmmoIsOn(false);
            this.botState.setEaterIsOn(false);
            this.botState.setPotterIsOn(false);
            this.botState.setHealerIsOn(false);

            this.changeScreen(this.menuScreen);

        } else {

            this.changeScreen(this.featuresScreen);

        }
    }
}
